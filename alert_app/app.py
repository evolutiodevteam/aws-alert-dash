#!/usr/bin/env python3

import aws_cdk as cdk

from alert_app.alert_app_stack import AlertAppStack


app = cdk.App()
AlertAppStack(app, "alert-app")

app.synth()
